//
//  AddRecipeViewController.swift
//  recipeParse
//
//  Created by Garrick McMickell on 10/29/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit
import Parse

class AddRecipeViewController: UIViewController {

    @IBOutlet var textField: UITextField!
    @IBOutlet var textView1: UITextView!
    @IBOutlet var textView2: UITextView!
    @IBOutlet var textField1: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveBtnTouched(_ sender: AnyObject) {
        var recipe = PFObject(className: "recipe")
        recipe["title"] = textField.text
        recipe["ingredients"] = textView1.text
        recipe["directions"] = textView2.text
        recipe["image"] = textField1.text
        recipe.saveInBackground {
            (success, error) -> Void in
            if (success) {
                print("Recipe saved.")
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissAddRecipeVC"), object: nil)
            } else {
                print(error)
            }
        }
    }
    
    @IBAction func cancelBtnTouched(_ sender: AnyObject) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissAddRecipeVC"), object: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
