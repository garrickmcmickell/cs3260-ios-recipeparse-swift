//
//  CollectionViewCell.swift
//  recipeParse
//
//  Created by Garrick McMickell on 10/29/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet var label: UILabel!
    @IBOutlet var imageView: UIImageView!
    
}
