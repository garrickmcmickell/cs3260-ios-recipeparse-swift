//
//  EditRecipeViewController.swift
//  recipeParse
//
//  Created by Garrick McMickell on 10/29/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit
import Parse

class EditRecipeViewController: UIViewController {
    
    @IBOutlet var textField: UITextField!
    @IBOutlet var textField1: UITextField!
    @IBOutlet var textView: UITextView!
    @IBOutlet var textView1: UITextView!
    
    var dictionary = [String: String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        textField.text = dictionary["title"]
        textField1.text = dictionary["image"]
        textView.text = dictionary["ingredients"]
        textView1.text = dictionary["directions"]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveBtnTouched(_ sender: AnyObject) {
        dictionary["title"] = textField.text
        dictionary["image"] = textField1.text
        dictionary["ingredients"] = textView.text
        dictionary["directions"] = textView1.text
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissEditRecipeVC"), object: dictionary)
        
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    
        
        var query = PFQuery(className:"recipe")
        query.getObjectInBackground(withId: dictionary["id"]!) {
            (recipe, error) -> Void in
            if error != nil {
                print(error)
            } else if let recipe = recipe {
                recipe["title"] = self.textField.text
                recipe["ingredients"] = self.textView.text
                recipe["directions"] = self.textView1.text
                recipe["image"] = self.textField1.text
                recipe.saveInBackground()
                
                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func deleteBtnTouched(_ sender: AnyObject) {
        var recipe = PFObject(withoutDataWithClassName: "recipe", objectId: dictionary["id"])
        recipe.deleteInBackground {
            (object, error) -> Void in
            if (error == nil){
                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
