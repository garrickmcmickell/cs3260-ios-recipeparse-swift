//
//  ViewController.swift
//  recipeParse
//
//  Created by Garrick McMickell on 10/29/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var collectionView: UICollectionView!
    
    var dictionaries = [[String: String]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.dismissAddRecipeVC), name: NSNotification.Name(rawValue: "dismissAddRecipeVC"), object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getData()
    }
    
    func getData() -> Void {
        dictionaries.removeAll()
        
        let query = PFQuery(className: "recipe")
        
        query.findObjectsInBackground {
            (objects, error) -> Void in
            
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) recipes.")
                // Do something with the found objects
                if let objects = objects {
                    for object in objects {
                        let recipe: [String: String] = ["title": object["title"] as! String, "ingredients": object["ingredients"] as! String, "directions": object["directions"] as! String, "image": object["image"] as! String, "id": object.objectId!]
                        self.dictionaries.append(recipe)
                        self.collectionView.reloadData()
                    }
                }
            } else {
                // Log details of the failure
                print("Did not pull recipes")
            }
        }
        
        print(dictionaries.count)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dictionaries.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
        let dictionary = dictionaries[indexPath.row]
        
        cell.label.text = dictionary["title"]
        
        if (dictionary["image"] != "") {
            let url = NSURL(string: dictionary["image"]!)
            let data = try? Data(contentsOf: url as! URL)
            cell.imageView.image = UIImage(data: data!)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "moveToRecipes", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "moveToRecipes") {
            let rvc: RecipeViewController = segue.destination as! RecipeViewController
            let ip = sender as! NSIndexPath
        
            rvc.dictionary = dictionaries[ip.row]
        }
    }
    
    func dismissAddRecipeVC(notif: Notification) -> Void {
        
        self.dismiss(animated: true) {
            
        }
    }
}

